var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var bigcommerceRouter = require('./routes/bigcommerce');
var mercadopagoRouter = require('./routes/mercadopago');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/bigcommerce', bigcommerceRouter);
app.use('/mercadopago', mercadopagoRouter);

module.exports = app;
