var express = require('express');
var router = express.Router();
router.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

var MercadopagoController = require('../controllers/MercadopagoController');
router.post('/webhook', MercadopagoController.webhook);
router.get('/orders', MercadopagoController.searchPayment);
router.get('/payments/:id', MercadopagoController.getPayment);
module.exports = router;
