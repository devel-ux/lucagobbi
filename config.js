let axios = require('axios');
const mode = "prod";
const bigcommerce = {
    access_token: 'c5ynifjztcuvnlgpk0plv7u092c24u7',
    client_id: 'k7ivhftvf5nnk5ss5ad5c2705qtctn4',
    client_secret: 'f1b2a21b80bcbfeecc8180e6635c4ff89293376a888e1ed030371f6e89e4b7ad',
    api_path_v3: 'https://api.bigcommerce.com/stores/jrlq43gl4u/v3/',
    api_path_v2: 'https://api.bigcommerce.com/stores/jrlq43gl4u/v2/'
}

const mercadopago = {
    private: "APP_USR-8157593035397827-070318-442a7ea777837e272c792a1715aa4a86-184924937"
}

const bcv3 = axios.create({
    baseURL: bigcommerce.api_path_v3,
    headers: {
        'X-Auth-Client': bigcommerce.client_id,
        'X-Auth-Token': bigcommerce.access_token,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    }
});

const bcv2 = axios.create({
    baseURL: bigcommerce.api_path_v2,
    headers: {
        'X-Auth-Client': bigcommerce.client_id,
        'X-Auth-Token': bigcommerce.access_token,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    }
});

module.exports = {
    mode,
    bigcommerce,
    mercadopago,
    bcv3,
    bcv2
}
