let config = require('../config');
let mercadopago = require('mercadopago');

let bcv2 = config.bcv2;
mercadopago.configure({
    sandbox: false,
    access_token: config.mercadopago.private
})

async function _pay(data) {
    try {
        let info = await mercadopago.payment.save(data);
        return info.response;
    } catch (errorOrder) {
        throw errorOrder
    }
}

async function _getPayment(payment) {
    try {
        const response = await mercadopago.payment.get(payment);
        return response;
    } catch (error) {
        throw error;
    }
}

async function getPayment(req, res) {
    try {
        payment = await _getPayment(req.params.id);
        res.status(200).send(payment.response);
    } catch (error) {
        if (error.isAxiosError) {
            res.status(error.response.data.status).send({error: error.response.data.title});
        } else {
            res.status(500).send({error: "Error interno 1"});
        }
    }
}

async function _searchPayment(config) {
    try {
        const response = await mercadopago.payment.search({
            qs: config
        },);
        return response.response.results;
    } catch (error) {
        throw error;
    }
}

async function searchPayment(req, res) {
    try {
        console.log(req.body.order);
        let payment = await _searchPayment({external_reference: req.body.order});
        console.log(payment);
        res.send(payment);
    } catch (error) {
        if (error.isAxiosError) {
            res.status(error.response.data.status).send({error: error.response.data.title});
        } else {
            res.status(500).send({error: "Error interno 1"});
        }
    }
}

async function webhook(req, res) {
    res.status(200).send({success: "Ok"});
    console.log(req.body);
    if (req.body.action == "payment.created") {
        let payment = await _getPayment(req.body.data.id)
        let order = payment.response.external_reference;
        let typePayment = '';
        if (payment.response.payment_type_id == "card") {
            typePayment = "Tarjeta - Mercadopago"
        } else if (payment.response.payment_type_id == "credit_card") {
            typePayment = "Tarjeta de crédito - Mercadopago"
        } else if (payment.response.payment_type_id == "ticket" && payment.response.payment_method_id == "oxxo") {
            typePayment = "OXXO - Mercadopago"
        } else {
            typePayment = "No reconocido - Mercadopago"
        }

        try {
            if (payment.response.status == "approved") {
                setTimeout(async () => {
                    let BigcommerceController = require('./BigcommerceController');
                    await BigcommerceController._updateOrder(
                        {
                            order: order,
                            data: {
                                status_id: 11,
                                payment_method: typePayment
                            }
                        }
                    )
                }, 10000)

            } else if (payment.response.status == "pending") {
                setTimeout(async () => {
                    let BigcommerceController = require('./BigcommerceController');
                    await BigcommerceController._updateOrder(
                        {
                            order: order,
                            data: {
                                status_id: 7,
                                payment_method: typePayment
                            }
                        }
                    )
                }, 10000)
            }
        } catch (e) {
            console.log(e);
        }
    }
}

module.exports = {
    _pay,
    webhook,
    getPayment,
    searchPayment
}
