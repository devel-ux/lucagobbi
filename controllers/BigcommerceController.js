let config = require('../config');
let MercadopagoController = require('./MercadopagoController');
let bcv3 = config.bcv3;
let bcv2 = config.bcv2;

async function _createOrder(checkout) {
    try {
        let requestOrder = await bcv3.post(`checkouts/${checkout}/orders`);
        let order = await requestOrder.data;
        return order;
    } catch (errorOrder) {
        throw errorOrder
    }
}

async function _getCheckout(checkout) {
    try {
        let requestCheckout = await bcv3.get(`checkouts/${checkout}`);
        return requestCheckout.data;
    } catch (errorCheckout) {
        throw errorCheckout
    }
}

async function _getOrder(order) {
    try {
        let requestOrder = await bcv3.get(`orders/${order}`);
        return requestOrder.data;
    } catch (errorOrder) {
        throw errorOrder
    }
}

async function getOrder(req, res) {
    try {
        order = _getOrder(req.body.id);
        res.status(200).send(order);
    } catch (error) {
        if (error.isAxiosError) {
            res.status(error.response.data.status).send({error: error.response.data.title});
        } else {
            res.status(500).send({error: "Error interno 1"});
        }
    }
}

async function _updateOrder(order) {
    try {
        await bcv2.put(`orders/${order.order}`, order.data);
        return order;
    } catch (errorOrder) {
        throw errorOrder
    }
}

async function createOrderOXXO(req, res) {
    if (!req.body.checkout) {
        res.status(402).send({
            error: 'El checkout es requerido'
        });
    }
    try {
        let order = await _createOrder(req.body.checkout);
        try {
            let checkoutInfo = await _getCheckout(req.body.checkout);

            data = {
                transaction_amount: parseFloat(checkoutInfo.data.grand_total),
                description: 'Orden Luca Gobbi - OXXO',
                payment_method_id: 'oxxo',
                external_reference: `${order.data.id}`,
                payer: {
                    email: checkoutInfo.data.cart.email
                }
            }
            
            let payment = await MercadopagoController._pay(data);
            await _updateOrder(
                {
                    order: order.data.id,
                    data: {
                        status_id: 7,
                        payment_method: "OXXO - Mercadopago"
                    }
                }
            )
            res.status(200).send(payment);
        } catch (error) {
            if (error.isAxiosError) {
                res.status(error.response.data.status).send({error: error.response.data.title});
            } else {
                res.status(500).send({error: error});
                console.log(error);
            }
        }
    } catch (error) {
        if (error.isAxiosError) {
            res.status(error.response.data.status).send({error: error.response.data.title});
        } else {
            res.status(500).send({error: error});
        }
    }
}

async function createOrder(req, res) {
    if (!req.body.checkout) {
        res.status(402).send({
            error: 'El checkout es requerido'
        });
    }
    try {
        console.log("ENTRANDO");
        console.log(req.body);
        let order = await _createOrder(req.body.checkout);
        try {
            console.log("TRY");
            let checkoutInfo = await _getCheckout(req.body.checkout);
            data = {
                transaction_amount: parseFloat(checkoutInfo.data.grand_total),
                token: req.body.token,
                description: 'Orden Luca Gobbi - Tarjeta',
                payment_method_id: req.body.payment_method_id,
                installments: parseInt(req.body.installments),
                external_reference: `${order.data.id}`,
                payer: {
                    email: checkoutInfo.data.cart.email
                }
            };
            
            let payment = await MercadopagoController._pay(data);
            if (payment.status == "approved") {
                await _updateOrder(
                    {
                        order: order.data.id,
                        data: {
                            status_id: 7,
                            payment_method: "TARJETA - Mercadopago"
                        }
                    }
                )
                res.status(200).send(payment);
            } else {
                res.status(402).send({warning: {status: payment.status, detail: payment.status_detail}});
            }
        } catch (error) {
            console.log(error);
            if (error.isAxiosError) {
                console.log("err0");
                res.status(error.response.data.status).send({error: error.response.data.title});
            } else {
                res.status(500).send({error: error});
                console.log(error);
            }
        }
    } catch (error) {
        if (error.isAxiosError) {
            console.log("err2");
            console.log(error);
            res.status(error.response.data.status).send({error: error.response.data.title});
        } else {
            res.status(500).send({error: error});
        }
    }
}

module.exports = {
    createOrder,
    createOrderOXXO,
    _updateOrder
}